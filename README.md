# Continuous Delivery Workflow
This a test repository for enforcing the usage of git branching strategy

## Description of the workflow
- Features are branched from master
- Features are merged to develop so that developers can test in the dev environement
- When Feature is completed, it is merged to QA branch so that QA Team can validate the work
- Once approved by QA, feature is merged to staging and source branch deleted
- Additional tests should be conducted on staging environment
- Feature(s) is then merged to master and tagged. Docker image is created with this specific tag and deployed to production.

## Possible enhancement
- Merging to QA and staging should ideally occur once status of User Story changes. Features could correspond to subtasks of the user story.


## Manual work
- `qa` branch should periodically be reset to master branch status