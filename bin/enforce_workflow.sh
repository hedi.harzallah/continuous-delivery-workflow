#!/bin/bash

if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" ]] && [[ ! $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ ^feature\/.+$ ]]
then
    echo "Merges to develop should only come from feature branches"; exit 1 ;
fi 

if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "qa" ]] && [[ ! $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ ^feature\/.+$ ]]
then
    echo "Merges to qa should only come from feature branches"; exit 1 ;
fi 

if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "staging" ]] && [[ ! $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ ^feature\/.+$ ]]
then echo "Merges to staging should only come from feature branches";
    exit 1 ;
fi 

if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master" ]] && [[ ! $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME == "staging" ]]
then
    echo "Merges to master should only come from staging branch"; exit 1
fi 

latest_master_commit=$(curl -s -H "JOB_TOKEN: ${CI_JOB_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/branches/master" | jq -r .commit.id)

nb_found=$(curl -H "JOB_TOKEN: ${CI_JOB_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${latest_master_commit}/refs" -s | jq '.[] | select(.name == "'$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'") | .name ' | wc -l)

if test $nb_found -eq 0 
then 
    echo "Latest commit from master not found in parents ! You should rebase from master !!"
    exit 2
fi
